# Ice Rink

This repo is dedicated for all things related to Ice Rink project. 

## Hot Water / Glycol Temperature Measuring

This system was built to help control the temperature of the ice in the Dunedin Ice Stadium.

This part of the project measures the temperature of the hot water system. This stops the ground underneath the ice from freezing.
And the temperature of the glycol system. This controls the temperature of the ice to make it softer or harder.

Both the hot water and glycol systems consist of two pipes each running under the ice, one leaving the pump room and one returning to the pump room.

To achieve this, we have 4 temperature sensors (ds18b20 Waterproof Version) and we place them between each pipe and its surrounding insulation. The sensors are connected to an 
ESP8266 (Custom board by LOLIN) via a Onewire bus. Once the ESP has successfully read the data, it then pushes the temperature data along with the sensors unique burned in serial number 
into our database using an HTTP POST request.

### To install the ESP board for Arduino IDE

1. Open Arduino IDE, click "File" at the top left corner. Then click "Preferences".

![example1](Images/ArduinoIDE1.PNG)

2. At the additional board manager to the "Additional Boards Manager URLs:" box, then click "OK". (http://arduino.esp8266.com/stable/package_esp8266com_index.json)

![example2](Images/ArduinoIDE2.PNG)

3. Next click on "Tools", "Board:", then "Boards Manager..."

![example3](Images/ArduinoIDE3.PNG)

4. Wait a second for the board manager to update, then search for "esp8266" and install the "esp8266" library by ESP8266 Community 

![example4](Images/ArduinoIDE4.PNG)

5. After this, click "Tools" then "Board:" once again. Select the corresponding ESP8266 to the one you are using.

### Uploading Sketch

Before you can program your ESP board you will need to install the required library (DallasTemperature).

The other libraries should be automatically installed.

1. Click on the "Tools" button, then "Manage libraries"

![example5](Images/ArduinoIDE5.PNG)

2. Wait a second for the library manager to update, then search for "DallasTemperature" and install the DallasTemperature library.

![example6](Images/ArduinoIDE6.PNG)

You will also need to get the "Connect.h" file out of the Sensitive repository. Place this file in the same directory as "IceRinkMain.ino".

You may then open the sketch and upload it to your ESP.

This Arduino IDE Sketch uses a deep sleep for the ESP8266. On most boards you will need to bridge pin16 to enable deep sleep to work correctly.


### Project Hardware

The DS18B20 temperature sensors are connected via a 1 wire bus. This feature lets us combine all of the sensors data connections, into 1 connection to the ESP.

![One Wire Diagram](Images/OneWireDiagram.PNG)

The connectors used are XLR 4 pin audio connectors.
The configuration for the connectors is:
Pin 1: Ground
Pin 2: Power (5v)
Pin 3: Data
Pin 4: Not Used

![XLR Plug](Images/XLRPlug.jpg)




