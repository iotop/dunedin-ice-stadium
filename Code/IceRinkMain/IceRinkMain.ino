///////////////////////////////////////////////////////
//// Code by IoT Project, Otago Polytechnic        ////
//// Designed to run on ESP8266 board              ////
//// https://gitlab.com/iotop/dunedin-ice-stadium  ////
///////////////////////////////////////////////////////

//libraries required
#include <OneWire.h>
#include <DallasTemperature.h>
#include <ESP8266HTTPClient.h>;
#include <ESP8266WiFi.h>;
//file with sensitive information
#include "Connect.h"

//define an instance of OneWire
#define ONE_WIRE_BUS 2
OneWire oneWire(ONE_WIRE_BUS);

//pass the oneWire object to DallasTemerature library 
DallasTemperature sensors(&oneWire);
//temp data for sensor serial number
char temp[10];

// WiFi timeout
int TIMEOUT = 30;               

// WiFi instead of EthernetClient
WiFiClient client;                   

//temp data for sensor reading
float readingTemp;

void setup(void)
{
  //start serial connection
  Serial.begin(74880);
  Serial.println(F("IceRink DEV"));

  // Connect WiFi
   Serial.printf("Connecting to %s", ssid);
   WiFi.begin(ssid, pass);
   //WiFi timeout counter
   int counter = 0;
   while (WiFi.status() != WL_CONNECTED) 
    {
        delay(1000);
        Serial.print(F("."));
        counter++;
        if (counter == TIMEOUT)
        {
          // 600e6 is 10 minutes of sleep in microseconds
          Serial.println(F("No WiFi! Deep sleep for 10 minutes. Bye!\n\n"));
          ESP.deepSleep(60e7); 
        }
    }
  
    // Connection info
    Serial.println(F("  Connect success!"));
    Serial.print(F("Local IP address: "));
    Serial.println(WiFi.localIP());

  //initialize oneWire bus 
  sensors.begin();
}

void loop(void)
{ 
  Serial.print(F("Requesting temperatures..."));
  sensors.requestTemperatures();
  Serial.println(F("DONE"));
  //temporary variable of DeviceAddress 
  DeviceAddress IDtemp;

  //Loop to get temperature data and call poster method
  for (int i = 0; i <= 3; i++)
  {
    sensors.getAddress(IDtemp, i);
    readingTemp = sensors.getTempCByIndex(i);
    Serial.println(readingTemp);

    //Changes Sensor serial number from, byte[] Hex -> Char[] ASCII
    array_to_string(IDtemp, 8, temp);
    Serial.println(temp);
    //calls method to send data via http POST 
    poster();
    readingTemp = 0;
  }

  //checking current SRAM use
  Serial.println("Current SRAM in use is : ");
  Serial.print(ESP.getFreeHeap(), DEC);
  Serial.flush();
  //10 minutes sleep in microseconds
  ESP.deepSleep(60e7);  
}

//Posts data via http POST
void poster()
{ 
   if(WiFi.status()== WL_CONNECTED)    // Check WiFi connection status
   {  
      // Declare object of class HTTPClient
      HTTPClient http;
    
      // Create POST data
      String POSTdata = "tmp=" + String(readingTemp) + "&devID=" + String(temp) + "&key=" + String(KEY);

      // Specify request destination
      http.begin("[INSERT INGEST URL HERE]");      

      // Specify content-type header
      http.addHeader("Content-Type", "application/x-www-form-urlencoded");    

      // Send the request
      int httpCode = http.POST(POSTdata); 

      // Get the response payload
      String payload = http.getString();          

      // Print HTTP return code
      Serial.println(httpCode); 

      // Print request response payload
      Serial.println(payload);    

      // Close connection
      http.end();
      }
   
   else
   {
      Serial.println(F("Problem with WiFi connection"));   
   }
}

//Changes Sensor serial number(device ID) from, byte[] Hex -> Char[] ASCII
void array_to_string(byte array[], unsigned int len, char buffer[])
{
    for (unsigned int i = 0; i < len; i++)
    {
        byte nib1 = (array[i] >> 4) & 0x0F;
        byte nib2 = (array[i] >> 0) & 0x0F;
        buffer[i*2+0] = nib1  < 0xA ? '0' + nib1  : 'A' + nib1  - 0xA;
        buffer[i*2+1] = nib2  < 0xA ? '0' + nib2  : 'A' + nib2  - 0xA;
    }
    buffer[len*2] = '\0';
}
