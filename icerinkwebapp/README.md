# This is the ReadME for the web application side. do not delete but edit.

## Django environment using Anaconda
1. If you dont have Anaconda, install it.
2. Open Anaconda Navigator and select Environments on the left Tab
3. Click 'create' and enter a name you would like to use for this project virtual environment.  Click on 'packages' and select Python 3.6 or more.  Click 'Create'.
4. After the creation of virtual environment, it will appear in the list below base(root) option.  Select your newly created environment and after it finishes loading, left click on the triangle icon.  This should bring up two options; select Open Terminal.
![anaconda image](https://i.ibb.co/nrjXm2M/anaconda.png)

## Database Settings 
1. Depending on what database you are using, you will need to change the settings in the settings.py file. Currently set to mysql. 

## Run Locally
1. In the Anaconda terminal you just opened, change directory to where the manage.py file sits 
> cd icerinkwebapp
2. In the command line you need to run migrations first 
> python manage.py migrate 
3. Once you have migrated your database you can now run the command
> python manage.py runserver 
4. View the page on localhost:8000 