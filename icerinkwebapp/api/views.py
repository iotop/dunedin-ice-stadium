from django.shortcuts import render
from django.http import HttpResponse
from .models import IceRinkData
# Create your views here.
def home(request):
   
    icedata = IceRinkData.objects.all()  #this is collecting all the data from the icerink database 
    
    context = {'icedata':icedata} #and sending it to the webpage as a dict 
    return render (request,'home.html',context)