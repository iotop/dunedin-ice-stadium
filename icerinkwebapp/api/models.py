from django.db import models

# Create your models here.
class IceRinkData(models.Model):
    pID = models.IntegerField(null=False, primary_key=True, unique=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    devID = models.CharField(default="devID", max_length=50)
    tmp = models.FloatField(default=0.00)
    def __str__(self):
        """A string representation of the model."""
        return f"{self.pID} - {self.devID}"