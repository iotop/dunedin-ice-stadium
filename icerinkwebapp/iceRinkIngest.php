<?php
    include 'connec.inc.php';	// Database Credentials

    if ($_POST['key'] == $pass)        // If pass correct
	{
        $tmp = $_POST['tmp'];       // Gathers user input
        strip_tags($tmp);             // Strips any tags from user input (Security)
        $devID = $_POST['devID'];       // Gathers user input
        strip_tags($devID);             // Strips any tags from user input (Security)

        $insertQuery="INSERT INTO op_icerink (pID, timestamp, devID, tmp) VALUES (NULL, CURRENT_TIMESTAMP, :devID, :tmp)"; // Template SQL Query
        $stmt =$pdo->prepare($insertQuery);
        $stmt->bindParam(':devID', $devID);
        $stmt->bindParam(':tmp', $tmp);

        $stmt->execute();       // Inserts row into table
    }
?>
